﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[Serializable]
public class DialogAnswer : ScriptableObject {
    [HideInInspector] public DialogStatement From;
    public DialogStatement To;

    [HideInInspector] public int Id;
    [HideInInspector] public Rect BoundingRect;

    public List<DialogAnswerCondition> conditions; 

    public string title;
    public DialogContainer parent;

    

    public void OnEnable() {
        title = title ?? "";
        conditions = conditions ?? new List<DialogAnswerCondition>();
        //hideFlags = HideFlags.HideAndDontSave;
    }

    public bool DoesPassConditions() {
        return conditions.TrueForAll(c => c.DoesPassCondition(parent.Properties.Find(p => p.Name == c.propertyName)));
    }

}