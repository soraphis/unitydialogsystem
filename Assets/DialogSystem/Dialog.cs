﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Soraphis.Lib;
using UnityEngine;


public class Dialog : MonoBehaviour {
    public DialogContainer Container;
    [HideInInspector] public DialogStatement currentStatement;

    private void Start() {
        currentStatement = Container.StartingPoint;
    }
    

    public DialogAnswer[] Answers {
        get {
            return Container.Answers.FindAll(a => a.From == currentStatement).ToArray();
        }
    }

    public DialogAnswer Answer {
        set {
            if(!Container.Answers.Contains(value)) throw new ArgumentException();
            currentStatement = value.To;
        }
    }
}
