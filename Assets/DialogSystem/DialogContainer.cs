﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Soraphis.Lib;
using UnityEditor;
using UnityEngine;

[Serializable]
[CreateAssetMenu]
public class DialogContainer : ScriptableObject {

    public string DialogName;

    public DialogStatement StartingPoint;
    [SerializeField] public List<DialogStatement> Statements;
    //    [SerializeField] public List<DialogAnswer> Answers;

    [HideInInspector] public List<DialogProperty> Properties;

    public List<DialogAnswer> Answers {
        get {
            return Statements.SelectMany(s => s.Answers).ToList();
        }
    }

    public void OnEnable() {
        DialogName = DialogName ?? "";
        Statements = Statements ?? new List<DialogStatement>();
        Properties = Properties ?? new List<DialogProperty>();
        //Answers = Answers ?? new List<DialogAnswer>();
    }

}
