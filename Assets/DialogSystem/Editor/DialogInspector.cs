﻿using System;
using UnityEngine;
using System.Collections;
using System.Diagnostics;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(DialogContainer))]
public class DialogInspector : Editor {

    private DialogProperty.Type type = DialogProperty.Type.BOOL;
    private string newkey;
    private int newvalueInt;
    private bool newvalueBool;
    private float newvalueFloat;

    public ReorderableList list;
    public DialogContainer container; 

    public void OnEnable() {
        container = (DialogContainer)target;
        list = new ReorderableList(container.Properties, typeof(DialogProperty), false, true, true, true);
        list.drawElementCallback += OnDrawElement;
        list.drawHeaderCallback = rect => EditorGUI.LabelField(rect, "Properties");
    }

    private void OnDrawElement(Rect rect, int index, bool isActive, bool isFocused) {
        DialogProperty property = container.Properties[index];
        float x = rect.x;

        property.type = (DialogProperty.Type)EditorGUI.EnumPopup(new Rect(x, rect.y, 60, EditorGUIUtility.singleLineHeight), property.type); x += 65;
        property.Name = EditorGUI.TextField(new Rect(x, rect.y, 100, EditorGUIUtility.singleLineHeight), property.Name); x += 105;

        float propertyWidth = rect.xMax - x;

        if (property.type == DialogProperty.Type.BOOL) property.boolValue = EditorGUI.Toggle(new Rect(x, rect.y, propertyWidth, EditorGUIUtility.singleLineHeight), property.boolValue);
        else if (property.type == DialogProperty.Type.INT) property.intValue = EditorGUI.IntField(new Rect(x, rect.y, propertyWidth, EditorGUIUtility.singleLineHeight), property.intValue);
        else if (property.type == DialogProperty.Type.FLOAT) property.floatValue = EditorGUI.FloatField(new Rect(x, rect.y, propertyWidth, EditorGUIUtility.singleLineHeight), property.floatValue);
        else {
            EditorGUILayout.TextField("cant parse value type");
        }
    }

    public override void OnInspectorGUI() {
        container = (DialogContainer) target;
        container.DialogName = EditorGUILayout.TextField("DialogName:", container.DialogName);

        list.DoLayoutList();

       container.Statements.ForEach((s) => s?.OnGUI());
        
    }
}

[CustomEditor(typeof(DialogStatement))]
public class DialogStatementInspector : Editor {

    public override void OnInspectorGUI() {
        DialogStatement statement = (DialogStatement)target;
        EditorGUILayout.BeginHorizontal();
        statement.name = EditorGUILayout.TextField("Dialog Statement", statement.name);
        EditorGUILayout.LabelField(statement.Id.ToString(), GUILayout.MaxWidth(30));
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.LabelField(name);
        statement.speaker = EditorGUILayout.TextField("Speaker Name", statement.speaker);
        statement.text = EditorGUILayout.TextArea(statement.text);
        statement.Answers.ForEach(DialogAnswerInspector.Draw);
        EditorGUILayout.Space();

    }
}